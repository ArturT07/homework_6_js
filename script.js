function createNewUser() {
    const newUser = {};
    let firstName = prompt("Enter name:");
    while (!firstName || !isNaN(firstName)) {
        firstName = prompt("Enter name:");
    }
    console.log("Name: " + firstName);

    let lastName = prompt("Enter Surname:");
    while (!lastName || !isNaN(lastName)) {
        lastName = prompt("Enter surname:");
    }
    console.log("Surname: " + lastName);

    let birthdayInput = prompt("Enter date of birth (dd.mm.yyyy):");
    while (!isValidDate(birthdayInput)) {
        birthdayInput = prompt("Enter a valid date of birth (dd.mm.yyyy):");
    }
    console.log("Birthday: " + birthdayInput);

    const [day, month, year] = birthdayInput.split(".");
    const birthday = new Date(`${year}-${month}-${day}`);
    newUser.firstName = firstName;
    newUser.lastName = lastName;
    newUser.birthday = birthday;

    newUser.getLogin = function() {
        const firstInitial = this.firstName.charAt(0).toUpperCase();
        const login = firstInitial + this.lastName.toLowerCase();
        return login;
    }
    newUser.getAge = function() {
        const today = new Date();
        const age = today.getFullYear() - this.birthday.getFullYear();
        return age;
    }
    newUser.getPassword = function() {
        const firstInitial = this.firstName.charAt(0).toUpperCase();
        const password = firstInitial + this.lastName.toLowerCase() + this.birthday.getFullYear();
        return password;
    }
    return newUser;
}

function isValidDate(dateString) {
    const [day, month, year] = dateString.split(".");
    const dateObject = new Date(`${year}-${month}-${day}`);
    return dateObject instanceof Date && !isNaN(dateObject);
}
const user = createNewUser();

const age = user.getAge();
console.log("Age:", age);
const login = user.getLogin();
console.log("Login:", login);
const password = user.getPassword();
console.log("Password:", password);
